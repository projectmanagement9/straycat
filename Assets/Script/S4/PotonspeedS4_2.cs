﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotonspeedS4_2 : MonoBehaviour
{
    public float Ballspeed;
    public void FixedUpdate()
    {
        transform.Translate(Ballspeed, 0f, 0f);//레이져의 이동속도
        Invoke("DestroyBall2", 39f);//24.5초뒤
    }
    public void DestroyBall2()
    {
        Destroy(gameObject);//레이져을 삭제한다.
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            Destroy(gameObject);//플레이어한테 닿으면 삭제된다.
    }
}

