﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findball2 : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            PotonAttackS4_2 raiser = this.GetComponent<PotonAttackS4_2>();
            raiser.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            PotonAttackS4_2 raiser = this.GetComponent<PotonAttackS4_2>();
            raiser.enabled = false;
        }
    }
}


