﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Chang_boss : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 납치범에 닿을 시
        {
            SceneManager.LoadScene("Stage4Clear");//스테이지4클리어씬으로 이동
        }
    }
}
