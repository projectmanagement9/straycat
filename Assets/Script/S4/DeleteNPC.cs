﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteNPC : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            Invoke("destroyNPC",2.5f);
        }
    }

    void destroyNPC()
    {
        Destroy(gameObject);
    }
}
