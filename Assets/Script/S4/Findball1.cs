﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findball1 : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            PotonAttackS4_1 raiser = this.GetComponent<PotonAttackS4_1>();
            raiser.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            PotonAttackS4_1 raiser = this.GetComponent<PotonAttackS4_1>();
            raiser.enabled = false;
        }
    }
}


