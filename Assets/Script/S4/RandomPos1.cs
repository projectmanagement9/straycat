﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPos1 : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            GameObject MovePos = GameObject.Find("player1");
            MovePos.transform.position = this.transform.position + new Vector3(2f, 4.3f, 0);
        }
    }
}
