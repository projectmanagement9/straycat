﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
    
	void Update () {
        if (SceneManager.GetActiveScene().name == "GameOver"&&Input.GetKeyDown(KeyCode.Space))//현재 stage1이고 스페이스바를 누를 시
        {
            SceneManager.LoadScene("Stage1");//Stage1에서 다시 시작한다.
        }
        if (SceneManager.GetActiveScene().name == "GameOver2" && Input.GetKeyDown(KeyCode.Space))//현재 stage2이고 스페이스바를 누를 시
        {
            SceneManager.LoadScene("Stage2");//Stage2에서 다시 시작한다.
        }
        if (SceneManager.GetActiveScene().name == "GameOver3" && Input.GetKeyDown(KeyCode.Space))//현재 stage3이고 스페이스바를 누를 시
        {
            SceneManager.LoadScene("Stage3");//Stage3에서 다시 시작한다.
        }
        if (SceneManager.GetActiveScene().name == "GameOver4" && Input.GetKeyDown(KeyCode.Space))//현재 stage4이고 스페이스바를 누를 시
        {
            SceneManager.LoadScene("Stage4");//Stage4에서 다시 시작한다.
        }
    }
}
