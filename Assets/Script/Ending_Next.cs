﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Ending_Next : MonoBehaviour{

    public nextEnd currentType;
    Vector3 defaultSize;//원래 버튼
    public AudioSource btnclicksource;
    public AudioSource btnsource;
    public GameObject one;
    public GameObject two;
    public GameObject three;
    public float count =0;
    public float count2 = 0;
    void Start () {
		
	}
    public void OnBtnClick()
    {
        switch (currentType)
        {
            case nextEnd.next:
                btnclicksource.Play();
                this.count = this.count + 1;
                if (count == 1)
                {
                    one.SetActive(true);
                }
                if (count == 2)
                {
                    two.SetActive(true);
                }
                if (count == 3)
                {
                    three.SetActive(true);
                }
                if (count > 3)
                {
                    count = 0;
                }
                    break;

            case nextEnd.back:
                btnclicksource.Play();
                count2 = count2 + 1;

                if (count2 == 3)
                {
                    one.SetActive(false);
                }
                if (count2 == 2)
                {
                    two.SetActive(false);
                }
                if (count2 == 1)
                {
                    three.SetActive(false);
                }
                if (count2 > 3)
                {
                    count2 = 0;
                }
                break;

            case nextEnd.quit:
                btnclicksource.Play();
                Invoke("reCall",2);
                break;
        }
    }
    void reCall()
    {
        SceneManager.LoadScene("Start1");//Start씬으로 전환된다.
    }
}
