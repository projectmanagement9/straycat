﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public static Score instance;//어디서나 접근할 수 있도록 정적으로 자기자신을 저장할 변수 선언
    public Text scoretext;//점수를 표시하는 Text객체를 에디터에서 받아옵니다.
    private int score = 0;//점수를 관리

    private void Awake()
    {
        if (!instance)//정적으로 자신(Score)을 체크
            instance = this;//정적으로 자신(Score)을 저장
    }

    public void AddScore(int numbers)
    {
       score = score + numbers;
       scoretext.text = "점수 : " + score;
    }
}
