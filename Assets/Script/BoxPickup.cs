﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxPickup : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))//접촉한 오브젝트의 Tag가 Player일때만 반응
        {
            Score.instance.AddScore(100);//아이템에 닿을 시 점수가 100점 상승
            Destroy(this.gameObject);
        }
    }
}
