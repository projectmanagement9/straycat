﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public float speed;//속도 조절

    bool isLeft = true;//방향 판단
	void Start () {
		
	}

	void Update () {
        transform.Translate(Vector2.left * speed * Time.deltaTime);//적의 이동
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "endPoint")
        {
            if (isLeft)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                isLeft = false;
            }//왼쪽방향으로 이동 중일때 충돌하면 오른쪽방향으로 돈다.
            else {
                transform.eulerAngles = new Vector3(0, 0, 0);
                isLeft = true;
            }
        }
    }
}
