﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnemy : MonoBehaviour {
    public float speed;//속도 조절

    bool isRight = true;//방향 판단

    public void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);//적의 이동
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "endPoint")
        {
            if (isRight)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                transform.Translate(0,-0.5f,0);
                isRight = false;
            }//왼쪽방향으로 이동 중일때 충돌하면 오른쪽방향으로 돈다.
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                transform.Translate(0, 0.5f, 0);
                isRight = true;
            }
        }
    }
}
