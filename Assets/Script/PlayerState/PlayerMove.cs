﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
    public float speed = 1;
    public float jump_speed;   //점프 높이 조절(수동)
    public bool jump = false;
    private SpriteRenderer spriteRenderer;
    public AudioSource effectsource;
    public GameObject explosion;
    public AudioSource shotmesource;//피격시 효과음
    public GameObject damageimage;
    public new Rigidbody2D rigidbody2D;
    public int count = 0;
    public GameObject dash;//특수능력
    public AudioSource btnctrlsource;//특수능력 시전 시 효과음

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();//투명색을 조절하기 위해 스프라이트렌더러를 불러온다.
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        HPbar call = GameObject.Find("Canvas").GetComponent<HPbar>();//HPbar를 가져온다.
        jump = false;//콜라이더끼리 충돌시 점프 불가 상태가 됨.
        if (collision.gameObject.tag == "Enemy")
        {//충돌한 오브젝트가 장애물이나 적일때

            if (call.currenthp <= 100 && call.currenthp >0)
            {
                shotmesource.Play();
                call.currenthp -= 20;//피격시 체력 20감소
                explosion.SetActive(true);
                spriteRenderer.color = new Color(1, 1, 1, 0.4f);//투명도 40%
                Invoke("offDamaged", 1);
                damageimage.SetActive(true);
            }
            else
            {
                if (SceneManager.GetActiveScene().name == "Stage1")//현재 Stage1이면
                    SceneManager.LoadScene("GameOver");//게임오버씬으로 전환된다.
                if (SceneManager.GetActiveScene().name == "Stage2")//현재 Stage2이면
                    SceneManager.LoadScene("GameOver2");//게임오버2씬으로 전환된다.
                if (SceneManager.GetActiveScene().name == "Stage3")//현재 Stage3이면
                    SceneManager.LoadScene("GameOver3");//게임오버3씬으로 전환된다.
                if (SceneManager.GetActiveScene().name == "Stage4")//현재 Stage4이면
                    SceneManager.LoadScene("GameOver4");//게임오버4씬으로 전환된다.
            }
        }
        
    }

    public void offDamaged()
    {
        spriteRenderer.color = new Color(1, 1, 1, 1);//원래 색
        explosion.SetActive(false);
        damageimage.SetActive(false);
    }

    public void offJumpItem()
    {
        jump_speed = 170;//점프력 초기화(JumpItem스크립트용)
    }
    public void offSpeedItem()
    {
        speed = 1;
    }
    public void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))//왼쪽이동
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;//flip으로 방향전환
            transform.Translate(-1.3f*speed*Time.deltaTime, 0f, 0f);//이동속도
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))//오른쪽이동
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            transform.Translate(1.3f * speed * Time.deltaTime, 0f, 0f);//이동속도
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))//점프
        {
            if (jump != true)
            {
                effectsource.Play();
                rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
                rigidbody2D.AddForce(Vector3.up * jump_speed);//AddForce는 오브젝트에 가속도를 준다.'F=ma'
                jump = true;
            }
        }
        if (SceneManager.GetActiveScene().name == "Stage4")//현재 Stage4이면
        {    
            if (count == 0)
            {
                dash.gameObject.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.RightControl) || Input.GetKeyDown(KeyCode.LeftControl))//특수능력을 쓸 수 있다.
            {
                count = count+1;
                if (count == 1)
                {
                    btnctrlsource.Play();
                    dash.gameObject.SetActive(true);
                    Invoke("dashOff",0.45f);
                    if (this.gameObject.GetComponent<SpriteRenderer>().flipX == false)
                        transform.Translate(2f, 0.1f, 0f);
                    dash.gameObject.GetComponent<SpriteRenderer>().flipX = false;
                    dash.gameObject.transform.position = this.transform.position + new Vector3(-0.74f, -0.1f, 0f);
                    if (this.gameObject.GetComponent<SpriteRenderer>().flipX == true)
                    {
                        transform.Translate(-2f, 0.1f, 0f);
                        dash.gameObject.GetComponent<SpriteRenderer>().flipX = true;
                        dash.gameObject.transform.position = this.transform.position + new Vector3(0.74f, -0.1f, 0f);
                    }
                }

            }
        }
    }
    void dashOff()
    {
        dash.gameObject.SetActive(false);
        Invoke("dashDelay", 7f);//스킬사용 후 대기시간

    }
    void dashDelay()
    {
        count = 0;
    }
}

