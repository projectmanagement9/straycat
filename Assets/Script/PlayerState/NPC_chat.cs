﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_chat : MonoBehaviour {

    public GameObject chating;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))//접촉한 오브젝트의 Tag가 Player일때만 반응
        {
            chating.SetActive(true);
            Invoke("Endchat", 3);
        }
 
    }

    void Endchat() {
        chating.SetActive(false);
    }
}
