﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class NPCSentence : MonoBehaviour {

    public string[] sentences;//대화 배열 선언

    private void OnMouseDown()
    {
        if (DialogueManager.instance.dialoguegroup.alpha == 0)
        DialogueManager.instance.Ondialogue(sentences);
    }//입력한 대화 배열을 DialogueManager의 instance에 할당

}
