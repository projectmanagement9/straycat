﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {


    private void OnCollisionEnter2D(Collision2D collision)//콜라이더끼리 충돌 시(물리적)
    {
        if (collision.gameObject.CompareTag("Player"))//Player태그를 가진 게임 오브젝트이면
        {
            GameManager.Instance.GameOver();//GameManager스크립트의 GameOver함수를 가져온다.
        }
    }

}
