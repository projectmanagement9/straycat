﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject menuSet;
    public GameObject MainmenuSet;
    public AudioSource btnclicksource;

    #region singleton
    private static GameManager _instance;
    public static GameManager Instance//싱글톤 패턴은 Instance키워드를 써줘야 한다.
    {
        get {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();//게임매니저 오브젝트를 찾는다.
            }
            return _instance;
        }
    }
    #endregion//싱글톤 패턴(하나의 전역변수로 해당 인스턴스가 클래스를 읽는 방법) 사용
    public void GameOver()
    {
        if (SceneManager.GetActiveScene().name == "Stage1")//현재 Stage1이면
            SceneManager.LoadScene("GameOver");//게임오버씬으로 전환된다.
        if (SceneManager.GetActiveScene().name == "Stage2")//현재 Stage2이면
            SceneManager.LoadScene("GameOver2");//게임오버2씬으로 전환된다.
        if (SceneManager.GetActiveScene().name == "Stage3")//현재 Stage3이면
            SceneManager.LoadScene("GameOver3");//게임오버3씬으로 전환된다.
        if (SceneManager.GetActiveScene().name == "Stage4")//현재 Stage3이면
            SceneManager.LoadScene("GameOver4");//게임오버4씬으로 전환된다.
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))//Space 버튼을 누를 시
        {//서브 메뉴 생성
            PlayerMove call = GameObject.Find("player1").GetComponent<PlayerMove>();//PlayerMove스크립트를 가져온다
            if (menuSet.activeSelf)//서브메뉴 오브젝트의 활성화여부(여기서는 비활성화된 상태)
            {
                Time.timeScale = 1;//프레임의 처리속도가 1이다.(평소상태)
                menuSet.SetActive(false);//서브 메뉴 비활성화
               call. gameObject.GetComponent<PlayerMove>().enabled = true;
            }
            else
            {
                Time.timeScale = 0;//프레임의 처리속도가 0이다.(모든 객체 일시정지)
                menuSet.SetActive(true);//서브 메뉴 활성화
                call.gameObject.GetComponent<PlayerMove>().enabled=false;
            }
        }
            
    }

    public void Continue()
    {
        btnclicksource.Play();
        Time.timeScale = 1;//프레임의 처리속도가 1이다.(평소상태)
        if (SceneManager.GetActiveScene().name == "Stage1")//현재 Stage1이면
            SceneManager.LoadScene("Stage1");//진행중인 스테이지1 화면으로 전환
        if (SceneManager.GetActiveScene().name == "Stage2")//현재 Stage2이면
            SceneManager.LoadScene("Stage2");//진행중인 스테이지2 화면으로 전환
        if (SceneManager.GetActiveScene().name == "Stage3")//현재 Stage3이면
            SceneManager.LoadScene("Stage3");//진행중인 스테이지3 화면으로 전환
        if (SceneManager.GetActiveScene().name == "Stage4")//현재 Stage4이면
            SceneManager.LoadScene("Stage4");//진행중인 스테이지4 화면으로 전환
    }

    public void Restart() {
        btnclicksource.Play();
        Time.timeScale = 1;//프레임의 처리속도가 1이다.(평소상태)
        SceneManager.LoadScene("Start1");//처음 화면으로 전환
    }

    public void Sound() {
        if (MainmenuSet.activeSelf)//사운드 버튼의 활성화여부(여기서는 비활성화된 상태)
        {
        }
        else
        {
            btnclicksource.Play();
            MainmenuSet.SetActive(true);//사운드UI 활성화  
        }
    }
    public void GameExit()
    {//서브메뉴의 '게임종료'클릭 시 게임종료
        btnclicksource.Play();
        Application.Quit();
    }

}