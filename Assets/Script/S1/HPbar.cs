﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPbar : MonoBehaviour {
    [SerializeField]
    public Slider hpbar;

    public float maxhp=100;//최대체력
    public float currenthp=100;//현재체력

	void Start () {
        hpbar.value = (float)currenthp / maxhp;//Full HP.
	}
	
	void Update () {
        UpdateHp();//상태가 계속 업데이트 된다.
    }

    public void UpdateHp()
    {
            hpbar.value = (float)currenthp / (float)maxhp;//피격시 후 현재 상태
        
    }
}
