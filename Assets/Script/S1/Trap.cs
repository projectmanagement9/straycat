﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {
    public GameObject HiddenTile;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            HiddenTile.SetActive(true);//숨겨진 타일이 나오게 한다.(활성화)
        }
    }

   void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            HiddenTile.SetActive(true);//숨겨진 타일이 나오게 한다.(활성화)
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
