﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTile : MonoBehaviour {
    public Transform startPos;//시작위치
    public Transform endPos;//끝나는위치
    public Transform desPos;//거리
    public float speed;

    void Start () {
        transform.position = startPos.position;
        desPos = endPos;
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            collision.transform.SetParent(transform);//player객체의 위치가 이 스크립트가 적용된 오브젝트의 자식으로 들어가짐.
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            collision.transform.SetParent(null);//자식 상태를 해제
        }
    }
    void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, desPos.position, Time.deltaTime * speed);//발판의 이동
        if (Vector2.Distance(transform.position, desPos.position) <= 0.05f)
        {
            if (desPos == endPos) desPos = startPos;
            else desPos = endPos;
        }
    }
}
