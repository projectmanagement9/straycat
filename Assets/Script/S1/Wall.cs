﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {
    public GameObject character;
    // Use this for initialization
    void Start () {
		
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            character.transform.position = new Vector3(-9.5f, -1.547f, 0);//이 좌표로 이동된다.
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
