﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class DialogueManager : MonoBehaviour,IPointerDownHandler
{//IPointerDownHandle인터페이스를 상속받는다.
    public Text dialogueText;//처음나올 텍스트
    public GameObject nextText;
    public CanvasGroup dialoguegroup;
    public Queue<string> sentences;//Queue(FiFO)를 이용한다. -> 텍스트를 하나씩 선입선출!!!
    public GameObject npcSentence;
    private string currentSentence;
    public float typingSpeed= 0.1f;
    private bool istyping;//대사가 끝나면 NextSentence함수를 불러올 수 있게 한다.
    public static DialogueManager instance;
    public AudioSource btnclicksource;
    public Canvas fadeout;

    private void Awake()
    {
        instance = this;
    }
    void Start () {
        sentences = new Queue<string>();
    }

    public void Ondialogue(string[] lines)
    {
        sentences.Clear();//Queue안에 있는 데이터를 비워준다.
        foreach (string line in lines)//큐 안에 들은 텍스트행의 line을 하나씩 읽어나간다.
        {
            sentences.Enqueue(line);
        }
        dialoguegroup.alpha = 1;
        dialoguegroup.blocksRaycasts = true;//마우스 이벤트를 감지한다.
        NextSentence();
    }

    public void NextSentence()
    {
        if (sentences.Count != 0)
        {
            currentSentence = sentences.Dequeue();/*Dequeue는 가장 먼저 들어온 데이터를 반환하고, 
            큐에서 해당 데이터를 제거합니다.*/
            istyping = true;
            nextText.SetActive(true);
            StartCoroutine(Typing(currentSentence));
        }
        else {
            dialoguegroup.alpha = 0;
            dialoguegroup.blocksRaycasts = false;//대화창을 종료한다.
            this.gameObject.SetActive(false);//텍스트창을 비활성화시킨다.
            GameObject.DestroyImmediate(npcSentence.GetComponent<NPCSentence>());
            //fox 오브젝트 안의 NPCSentence스크립트를 삭제한다.
            if (npcSentence.tag.Equals("NPC")==true) {//오브젝트의 tag명이 'NPC'일 경우
                fadeout.GetComponent<FadeScript>().Fade();
                Invoke("NextStage", 2);//2초 뒤 stage2로 넘어간다.
            }
        }
    }

    IEnumerator Typing(string line)/*대화창(dialogueText)의 text를 빈문자열로 초기화하고,
        */
    {
        dialogueText.text = "";
        foreach (char letter in line.ToCharArray()) {
            dialogueText.text += letter;
            yield return new WaitForSeconds(typingSpeed);//typingSpeed(0.1초)만큼 기다렸다가 실행된다.
        }
    }//ToCharArray함수는 문자열을 char형 배열로 변환해 줍니다.
	void Update () {
        //dialogeText==currentSentence 대사를 출력 후 끝.
        if (dialogueText.text.Equals(currentSentence))
        {
            istyping = false;
        }
	}
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!istyping)
        {
            NextSentence();
            btnclicksource.Play();
        }
    }

    public void NextStage() {
        if (SceneManager.GetActiveScene().name == "Stage1Clear")
            SceneManager.LoadScene("Stage2");//stage2씬 로드
        if (SceneManager.GetActiveScene().name == "Stage2Clear")
            SceneManager.LoadScene("Stage3");//stage3씬 로드
        if (SceneManager.GetActiveScene().name == "Stage3Clear")
            SceneManager.LoadScene("Stage4");//stage4씬 로드
        if (SceneManager.GetActiveScene().name == "Stage4Clear")
            SceneManager.LoadScene("Ending");//Ending씬 로드
    }
}
