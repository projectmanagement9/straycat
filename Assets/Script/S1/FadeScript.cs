﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeScript : MonoBehaviour {
    public Image Panel;
    float time = 0f;//시간누적
    float F_time = 1f;//페이드 아웃의 지속시간
    public void Fade()
    {
        StartCoroutine(FadeFlow());
    }
    IEnumerator FadeFlow()
    {
        Panel.gameObject.SetActive(true);//코루틴이 시작될 때 페이드를 활성화
        Color alpha = Panel.color;
        while (alpha.a<1f)
        {
            time += Time.deltaTime / F_time;//매 프레임의 deltaTime을 F_time으로 나눈 값을 time에 누적시킨다.
            alpha.a = Mathf.Lerp(0,1,time);//Mathf.Lerp으로 부드럽게 변하도록 만들어 준다.
            Panel.color = alpha;//alpha값을 매 프레임 이미지의 컬러 값에 대입해준다.
            yield return null;
        }

        yield return null;
    }
}
