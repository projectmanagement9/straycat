﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour {
    Inventory inventory;
    public int num;

    private void Start()
    {
        inventory = GameObject.Find("player1").GetComponent<Inventory>();
        //슬롯의 자식오브젝트가 없다면 isEmpty를 true로 반환한다.
        num = int.Parse(gameObject.name.Substring(gameObject.name.IndexOf("_")+1));
        /*SubString :시작위치부터 문자열을 잘라옵니다.
        indexof("찾을문자") :찾을 문자의 위치를 반환*/
    }
    private void Update()
    {
        if (transform.childCount <= 0)//자식오브젝트가 0이하이면
        {
            inventory.slots[num].isEmpty = true;
        }
    }
}
