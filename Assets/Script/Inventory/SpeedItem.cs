﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedItem : MonoBehaviour {
    public void Update()
    {
        if (Input.inputString == (transform.parent.GetComponent<Slot>().num + 1).ToString())
        //부모인 Slot의 num변수를 가져온다. + string형으로 형식을 맞춰주기 위해 ToString()사용
        {
            PlayerMove call = GameObject.Find("player1").GetComponent<PlayerMove>();//PlayerMove스크립트를 가져온다
            //아이템 사용
            call.speed = 2f;//이동속도가 2배로 증가
            call.Invoke("offSpeedItem", 10);//10초 후 원래 상태로 돌아온다.
            Destroy(this.gameObject);//사용한 Jump아이템 오브젝트 삭제
        }
    }
}
