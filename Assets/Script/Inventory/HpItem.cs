﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpItem : MonoBehaviour {


    void Update () {
        if (Input.inputString == (transform.parent.GetComponent<Slot>().num + 1).ToString())
        //부모인 Slot의 num변수를 가져온다. + string형으로 형식을 맞춰주기 위해 ToString()사용
        {
            //아이템 사용
            HPbar call = GameObject.Find("Canvas").GetComponent<HPbar>();//HPbar를 가져온다
            call.currenthp += 10;//HP 10 상승
            Destroy(this.gameObject);//사용한 HP아이템 오브젝트 삭제
        }
	}
}
