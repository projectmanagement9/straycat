﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public List<SlotData> slots = new List<SlotData>();//슬롯에 들어갈 오브젝트를 리스트로 관리해주기 위해 선언
    private int maxSlot = 3;//슬롯의 개수는 최대3개이다.(그 이상은 안 채워짐)
    public GameObject slotPrefab;
    private void Start()
    {
        GameObject slotPanel = GameObject.Find("Panel");//부모 오브젝트인 Panel을 찾는다.
        for (int i = 0; i < maxSlot; i++)//반복문을 이용해 슬롯을 생성하고  slots리스트에 담는다.
        {
            GameObject goes = Instantiate(slotPrefab,slotPanel.transform,false);//습득한 아이템의 위치는 slotPanel아래가 된다.
            //Instantiate는 게임 실행 도중에 게임오브젝트가 생성될 수 있게 해준다.
            goes.name = "Slot_" + i;//슬롯이름은 Slot_0,Slot_1,Slot_2
            SlotData slot = new SlotData();
            slot.isEmpty = true;
            slot.slotObj = goes;
            slots.Add(slot);
        }
    }
    void Update () {
		
	}
}
