﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameractrl : MonoBehaviour
{
    private GameObject go;

    void Start()
    {
        go = GameObject.Find("player1");//추적대상 객체
    }

    void Update()
    {
        Camera.main.transform.position = go.transform.position - Vector3.forward;//카메라가 대상을 따라감
    }
}