﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotonAttack : MonoBehaviour
{

    public GameObject potons;//레이져
    public Transform pos;
    private float count = 0;//총알이 복제되는 시간을 조절


    void Update()
    {
        if (count == 0)
            Invoke("Shoot", 0.01f);
    }
    void Shoot()
    {
        Instantiate(potons, pos.position, transform.rotation);//총알을 복제
        count = 1;//대기시간동안 총알이 계속 복사되는 것을 방지
        Invoke("raiserReady", 0.9f);//0.9초에 1번씩 raiserReady함수 실행
    }
    void raiserReady()
    {
        count = 0;
    }

}

