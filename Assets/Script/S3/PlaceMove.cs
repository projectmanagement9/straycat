﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceMove : MonoBehaviour {

    public GameObject object1;
    public GameObject good;
    public void OnCollisionStay2D(Collision2D collision)//콜라이더와 접촉하는 동안
    {
        if (collision.gameObject.CompareTag("Player"))
        {//충돌한 대상이 Player일때
            if (Input.GetKeyDown(KeyCode.S) || (Input.GetKeyDown(KeyCode.DownArrow)))//키보드 S나 아래 방향키를 누르면
            {
                GameObject Pos = GameObject.Find("player1");//player1을 찾아
                Pos.gameObject.transform.position = object1.transform.position+new Vector3(0,0.36f,0);//위치 변경
                good.SetActive(true);
                Invoke("goodOff",1);
            }
        }
    }
    void goodOff()
    {
        good.SetActive(false);
    }
}
