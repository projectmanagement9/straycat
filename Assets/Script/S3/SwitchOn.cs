﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchOn : MonoBehaviour {
    public Sprite Currentsprite;
    public Sprite Nextsprite;
    SpriteRenderer image;
    public GameObject call;
        
	void Start () {

        image = gameObject.GetComponent<SpriteRenderer>();
        image.sprite = Currentsprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        image.sprite = Nextsprite;
            call.SetActive(true);

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        image.sprite = Currentsprite;
    }

}
