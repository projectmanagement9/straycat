﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class potonspeed1 : MonoBehaviour {
    public float Ballspeed;
    public void FixedUpdate()
    {
        transform.Translate(Ballspeed, 0f, 0f);//레이져의 이동속도
        Invoke("DestroyBullet2", 3f);//3초뒤
    }
    public void DestroyBullet2()
    {
        Destroy(gameObject);//레이져을 삭제한다.
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            Destroy(gameObject);//플레이어한테 닿으면 삭제된다.
    }
}
