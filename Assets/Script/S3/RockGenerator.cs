﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockGenerator : MonoBehaviour {

    public GameObject rockPrefab;//바위 프리팹이 담긴다.
    public GameObject endposx;//최대 X좌표
    private float spawn = 0.5f;//대기시간
	private float delaytime =0;

	void Update () {
        this.delaytime += Time.deltaTime;//매 프레임마다의 시간 누적
        if (this.delaytime > this.spawn) {
            this.delaytime = 0;
            GameObject renew = Instantiate(rockPrefab) as GameObject;//프리팹을 복사해서 생성한다.
            float Xpos = Random.Range(transform.position.x, endposx.transform.position.x);//프리팹의 X축 위치는 지정된 범위 내이다.
            float Ypos = transform.position.y;//RockGenerator스크립트가 적용된 오브젝트의 위치가 프리팹의 Y축 위치
            renew.transform.position = new Vector3(Xpos, Ypos, 0);
        }
	}
}
