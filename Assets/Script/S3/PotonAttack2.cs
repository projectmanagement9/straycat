﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotonAttack2 : MonoBehaviour
{

    public GameObject potons2;//레이져
    public Transform pos2;
    private float count2 = 0;//총알이 복제되는 시간을 조절


    void Update()
    {
        if (count2 == 0)
            Invoke("Shoot2", 0.01f);
    }
    void Shoot2()
    {
        Instantiate(potons2, pos2.position, transform.rotation);//총알을 복제
        count2 = 1;//대기시간동안 총알이 계속 복사되는 것을 방지
        Invoke("raiserReady2", 1.5f);//1.5초에 1번씩 raiserReady함수 실행
    }
    void raiserReady2()
    {
        count2 = 0;
    }
}


