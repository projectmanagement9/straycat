﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rockfallcontrol : MonoBehaviour {

    public float selfDestroyRock;
	void Start () {
		
	}
	
	void Update () {
        transform.Translate(0,-0.04f,0);//바위가 떨어지는 속도

        if (transform.position.y < selfDestroyRock) {
            Destroy(gameObject);
        }
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Player")//플레이어에게 닿을때
            Destroy(gameObject);//삭제
    }
}
