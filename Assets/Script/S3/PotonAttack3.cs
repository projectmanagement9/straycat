﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotonAttack3 : MonoBehaviour
{

    public GameObject potons3;//레이져
    public Transform pos3;
    private float count3 = 0;//총알이 복제되는 시간을 조절


    void Update()
    {
        if (count3 == 0)
            Invoke("Shoot3", 0.01f);
    }
    void Shoot3()
    {
        Instantiate(potons3, pos3.position, transform.rotation);//총알을 복제
        count3 = 1;//대기시간동안 총알이 계속 복사되는 것을 방지
        Invoke("raiserReady3", 2f);//1.5초에 1번씩 raiserReady함수 실행
    }
    void raiserReady3()
    {
        count3 = 0;
    }
}



