﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchOn_2 : MonoBehaviour
{
    public Sprite Currentsprite;
    public Sprite Nextsprite;
    SpriteRenderer image;

    void Start()
    {
        image = gameObject.GetComponent<SpriteRenderer>();
        image.sprite = Currentsprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        MovingTile call = GameObject.Find("S3Tile_3_4").GetComponent<MovingTile>();//PlayerMove스크립트를 가져온다
        image.sprite = Nextsprite;
        call.enabled=true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        MovingTile call = GameObject.Find("S3Tile_3_4").GetComponent<MovingTile>();//PlayerMove스크립트를 가져온다
        image.sprite = Currentsprite;
        call.enabled=false;
    }

}

