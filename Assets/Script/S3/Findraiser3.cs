﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findraiser3 : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        PotonAttack3 raiser3 = this.GetComponent<PotonAttack3>();
        if (other.gameObject.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            raiser3.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        PotonAttack3 raiser3 = this.GetComponent<PotonAttack3>();
        if (other.gameObject.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            raiser3.enabled = false;
        }
    }
}



