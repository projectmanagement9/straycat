﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findraiser : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        PotonAttack raiser = this.GetComponent<PotonAttack>();
        if (other.gameObject.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            raiser.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        PotonAttack raiser = this.GetComponent<PotonAttack>();
        if (other.gameObject.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            raiser.enabled = false;
        }
    }
}

