﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumppack : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerMove call = GameObject.Find("player1").GetComponent<PlayerMove>();//PlayerMove스크립트를 가져온다
        call.jump_speed = 400;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        PlayerMove call = GameObject.Find("player1").GetComponent<PlayerMove>();//PlayerMove스크립트를 가져온다
        call.jump_speed = 170;
    }
}
