﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapoff : MonoBehaviour {
    public GameObject HiddenTileoff;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            HiddenTileoff.SetActive(false);//타일을 숨긴다.(비활성화)
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")//플레이어가 충돌 시
        {
            HiddenTileoff.SetActive(false);//타일을 숨긴다.(비활성화)
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
