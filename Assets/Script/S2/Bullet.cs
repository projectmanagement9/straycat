﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float Ballspeed;
	public void FixedUpdate () {
        transform.Translate(Ballspeed, 0f, 0f);//대포알의 이동속도
        Invoke("DestroyBullet",5f);//5초뒤
    }
    public void DestroyBullet()
    {
        Destroy(gameObject);//대포알을 삭제한다.
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            Destroy(gameObject);//플레이어한테 닿으면 삭제된다.
    }
}
