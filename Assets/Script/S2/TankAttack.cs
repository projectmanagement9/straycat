﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankAttack : MonoBehaviour {

    public GameObject bullet;//총알
    public GameObject boom;//폭발 이펙트
    public Transform pos;
    private float count = 0;//총알이 복제되는 시간을 조절

	void Start () {

    }
	
	void Update () {
        if(count==0)
        Invoke("Shoot",0.01f);
    }
    void Shoot()
    {
        Instantiate(bullet, pos.position, transform.rotation);//총알을 복제
        Instantiate(boom, pos.position, transform.rotation);//폭발 이펙트를 복제
        count = 1;//대기시간동안 총알이 계속 복사되는 것을 방지
        Invoke("Ready",2f);//2초에 1번씩 Ready함수 실행
    }
    void Ready()
    {
        count = 0;
    }
}
