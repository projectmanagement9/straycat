﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladderride : MonoBehaviour {
    public  bool isLadder;
    public Collider2D platformCollider;
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag=="Player")
        {
            
            isLadder = true;
        }
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//충돌한 오브젝트의 태그가 Player이면
        {
            isLadder = false;
            Physics2D.IgnoreCollision(other.GetComponent<Collider2D>(), platformCollider, false);

        }

    }
    public void FixedUpdate () {//Fixed는 일정시간동안 반복하는 구문으로 리지드바디 상태일때 주로 쓴다.
        GameObject sunghun = GameObject.Find("player1");
        if (isLadder)//isLadder이 true일때
        {
            sunghun.GetComponent<Animator>().enabled = true;//사다리를 타는 동안 애니메이션 실행
            sunghun.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;//플레이어의 모든 축을 고정한다.
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))//위쪽방향키 - 사다리이동
            {
                sunghun.GetComponent<Rigidbody2D>().gravityScale = 0;
                sunghun.transform.Translate(0f, 0.03f, 0f);
            }
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))//아래쪽방향키 - 사다리이동
            {
                sunghun.GetComponent<Rigidbody2D>().gravityScale = 0;//플레이어가 받는 중력값이 0이된다.(무중력)
                sunghun.transform.Translate(0f, -0.03f, 0f);
            }
        }
        else//값을 다시 되돌려준다.
        {
            sunghun.GetComponent<Rigidbody2D>().gravityScale = 1;
            sunghun.GetComponent<Rigidbody2D>().constraints = (RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation);
            sunghun.GetComponent<Animator>().enabled = false;//애니메이션 종료
        }
    }

    public void OnTriggerStay2D(Collider2D other)//접촉중인 콜라이더를 계속 반환한다.
    {
        if (other.CompareTag("Player"))
        {
            Physics2D.IgnoreCollision(other.GetComponent<Collider2D>(), platformCollider,true);//콜라이더끼리의 충돌을 무시한다.
        }
    }


}
