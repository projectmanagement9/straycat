﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Find : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        TankAttack tanks = this.GetComponent<TankAttack>();
        if (other.gameObject.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            tanks.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        TankAttack tanks = this.GetComponent<TankAttack>();
        if (other.gameObject.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            tanks.enabled = false;
        }
    }
}
