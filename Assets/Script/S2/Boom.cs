﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour {

	public void Update () {
        Invoke("DestroyBoom", 1.5f);//1.5초뒤
    }
    public void DestroyBoom()
    {
        Destroy(gameObject);//대포알을 삭제한다.
    }
}
