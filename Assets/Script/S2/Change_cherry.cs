﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Change_cherry : MonoBehaviour {

    void Start()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")//플레이어가 체리에 닿을 시
        {
            SceneManager.LoadScene("Stage2Clear");//스테이지2클리어씬으로 이동
        }
    }
}
