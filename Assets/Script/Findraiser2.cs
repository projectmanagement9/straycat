﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findraiser2 : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        PotonAttack2 raiser2 = this.GetComponent<PotonAttack2>();
        if (other.gameObject.CompareTag("Player"))//충돌한 대상이 Player일때
        {
            raiser2.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        PotonAttack2 raiser2 = this.GetComponent<PotonAttack2>();
        if (other.gameObject.CompareTag("Player"))//충돌지점에서 탈출할 때 
        {
            raiser2.enabled = false;
        }
    }
}


