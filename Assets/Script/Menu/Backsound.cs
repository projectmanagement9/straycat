﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backsound : MonoBehaviour {

    public GameObject sound;
    public AudioSource effectsource;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            effectsource.Play();
            sound.SetActive(false);//esc 누를 시 설명서 비활성화     
        }
    }
}
