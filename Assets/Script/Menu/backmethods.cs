﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class backmethods : MonoBehaviour {
    public GameObject methods;
    public AudioSource effectsource;

    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            effectsource.Play();
            methods.SetActive(false);//esc 누를 시 설명서 비활성화     
        }
    }
}
