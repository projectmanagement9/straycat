﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class subBtnType : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{
    /*마우스가 닿으면(IPointerEnterHandler) 버튼이 확대되고,
   떼지면(IPointerExitHandler) 버튼이 원래 상태로 돌아온다.*/

    public Transform buttonSize;//버튼 크기 키우기
    Vector3 defaultSize;//원래 버튼
    public AudioSource btnsource;

    private void Start()
    {
        defaultSize = buttonSize.localScale;//원래 버튼 사이즈를 확대된 사이즈로 초기화해준다.
    }//현 스크립트에서만 1번 실행되게 함.

    public void OnPointerEnter(PointerEventData eventData)//버튼 오브젝트에 마우스가 닿으면 OnPointerEnter이 호출된다.
    {
        btnsource.Play();
        buttonSize.localScale = defaultSize * 1.2f;//버튼 크키를 1.2배 키워준다
    }

    public void OnPointerExit(PointerEventData eventData)//버튼 오브젝트에서 마우스가 빠져나가면 호출된다.
    {
        buttonSize.localScale = defaultSize;//원래 사이즈로 돌아온다.
    }
}
