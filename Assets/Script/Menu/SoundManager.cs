﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource musicsource;//오디오 소스(배경음)를 public형 변수로 선언
    public AudioSource btnsource;//오디오 소스(버튼음)를 public형 변수로 선언
    public AudioSource effectsource;//단축키를 누를 때 발생하는 효과음
    public AudioSource btnclicksource;//버튼 클릭 시 효과음
    public AudioSource shotmesource;//피격시 효과음
    public AudioSource btnctrlsource;//특수능력 시전 시 효과음

    public void SetMusicVolume(float volume)
    {//슬라이더의 볼륨값을 받아서
        musicsource.volume = volume;//배경음의 볼륨 조절
    }

    public void SetButtonVolume(float volume)
    {//슬라이더의 볼륨값을 받아서
        btnsource.volume = volume;//버튼음의 볼륨 조절
        BtnSound();
        btnclicksource.volume = volume;//버튼 클릭음의 볼륨 조절
        BtnclickSound();
        btnctrlsource.volume = volume;
        BtnctrlSound();
    }
    public void BtnSound() {
        btnsource.Play();
    }
    public void BtnclickSound()
    {
        btnclicksource.Play();
    }
    public void BtnctrlSound()
    {
        btnctrlsource.Play();
    }
    public void SetEffectVolume(float volume)
    {//슬라이더의 볼륨값을 받아서
        effectsource.volume = volume;//효과음의 볼륨 조절
        EffectSound();
        shotmesource.volume = volume;//피격음의 볼륨 조절
        ShotmeSound();
    }
    public void EffectSound() {
        effectsource.Play();
    }
    public void ShotmeSound() {
        shotmesource.Play();
    }
}
