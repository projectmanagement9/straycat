﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pickup : MonoBehaviour {

    public GameObject slotItem;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))//접촉한 오브젝트의 Tag가 Player일때만 반응
        {
            Score.instance.AddScore(10);//아이템에 닿을 시 점수가 10점 상승
            Inventory inven = collision.GetComponent<Inventory> ();
            for (int i = 0; i < inven.slots.Count; i++)//isEmpty에서 빈곳을 찾는다.(빈 슬롯창)
            {
                if (inven.slots[i].isEmpty)//isEmpty가 true일때(슬롯이 가득찼을때)
                {
                    Instantiate(slotItem,inven.slots[i].slotObj.transform,false);//Instantiate(생성할 오브젝트,생성할 위치,False);
                    inven.slots[i].isEmpty = false;//더이상 못 들어감
                    Destroy(this.gameObject);//이후에 들어오는 오브젝트는 삭제된다.
                    break;//반복문 탈출
                }
            }
            
        }
    }
    void Update () {
		
	}
}
