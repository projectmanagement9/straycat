﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    public Slider progressbar;
    public Text loadtext;

    private void Start()
    {
        StartCoroutine(LoadScene());
    }
    IEnumerator LoadScene()
    {
        yield return null;
        AsyncOperation operation = SceneManager.LoadSceneAsync("Stage1");
        operation.allowSceneActivation = false;//로딩이 끝나도 동작하지 않고 멈춰있게 만든다.

        while (!operation.isDone)//슬라이더의 value값을 매 프레임 증가시킴
        {
            yield return null;
            if (progressbar.value < 4f)//value값이 4보다 작으면
            {
                progressbar.value = Mathf.MoveTowards(progressbar.value, 4f, Time.deltaTime);
            }
            else
            {
                loadtext.text = "스페이스바를 눌러서 꼬물이를 찾으러 떠나요!!!";//게임시작 전 출력되는 텍스트
            }

            if (Input.GetKeyDown(KeyCode.Space)&&progressbar.value>=4f)//value값이 4로 채워졌을 때 스페이스바를 누르면
            {
                operation.allowSceneActivation = true;//다음 씬(STAGE1)으로 전환
            }
        }
    }
}
