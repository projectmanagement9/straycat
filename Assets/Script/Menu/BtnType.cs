﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class BtnType : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler{
    /*마우스가 닿으면(IPointerEnterHandler) 버튼이 확대되고,
    떼지면(IPointerExitHandler) 버튼이 원래 상태로 돌아온다.*/
    public mButton currentType;
    public Transform buttonSize;//버튼 크기 키우기
    Vector3 defaultSize;//원래 버튼
    public GameObject MainmenuSet;
    public AudioSource btnsource;
    public AudioSource btnclicksource;

    private void Start()
    {
        defaultSize = buttonSize.localScale;//원래 버튼 사이즈를 확대된 사이즈로 초기화해준다.
    }//현 스크립트에서만 1번 실행되게 함.
    public void OnBtnClick()
    {
        switch (currentType)
        {
            case mButton.start:
                btnclicksource.Play();
                SceneManager.LoadScene("Loading");//마우스버튼을 클릭 시 로딩 씬으로 전환된다.(게임시작)
                break;
            case mButton.methods:
                if (MainmenuSet.activeSelf) { }//설명서 버튼의 활성화여부(여기서는 비활성화된 상태)
                else {
                    btnclicksource.Play();
                    MainmenuSet.SetActive(true);//설명서 활성화  
                }
                break;
            case mButton.story:
                if (MainmenuSet.activeSelf) { }//스토리 버튼의 활성화여부(여기서는 비활성화된 상태)
                else
                {
                    btnclicksource.Play();
                    MainmenuSet.SetActive(true);//스토리 활성화  
                }
                break;
            case mButton.sound:
                if (MainmenuSet.activeSelf) { }//사운드 버튼의 활성화여부(여기서는 비활성화된 상태)
                else
                {
                    btnclicksource.Play();
                    MainmenuSet.SetActive(true);//음량 조절 활성화  
                }
                break;
            case mButton.exit:
                Application.Quit();
                break;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)//버튼 오브젝트에 마우스가 닿으면 OnPointerEnter이 호출된다.
    {
        btnsource.Play();
        buttonSize.localScale = defaultSize * 1.2f;//버튼 크키를 1.2배 키워준다.
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        buttonSize.localScale = defaultSize;//원래 사이즈로 돌아온다.
    }
}
