- 안녕하세요. 해당 미니 프로젝트의 이름은 "집 나간 고양이"입니다.
- 대학교 2학년 때 유니티를 처음 배우면서 제작히였습니다.
- 자세한 설명은 제 블로그에서 확인해주시면 됩니다.
- OoD의 연구노트 블로그: [미니 프로젝트][Unity] '집 나간 고양이' 소개](https://ocean-of-data.tistory.com/category/%ED%8F%AC%ED%8A%B8%ED%8F%B4%EB%A6%AC%EC%98%A4/%EB%AF%B8%EB%8B%88%20%ED%94%84%EB%A1%9C%EC%A0%9D%ED%8A%B8)

![시작화면](/uploads/a393fc06a8d800b2639000ce759f0f2a/시작화면.jpg)
![image](/uploads/5ce787a8b7d88fe135a1a6c49dd99b71/image.png)
![image](/uploads/f4e2d1c06dc3747014d30d47a748898d/image.png)
![image](/uploads/bd6b52e9faad1b38295c2cccde9afe12/image.png)
![image](/uploads/2119c434e096249420ef3edaa6eee69d/image.png)
![Ending1](/uploads/ce1dbb83aa5b1e80bc4f8f02da610595/Ending1.png)
